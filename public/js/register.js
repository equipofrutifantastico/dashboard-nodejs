function __(id){
    return document.getElementById(id);
}

function checkName() {
    var name = __('name');
    if(!(/^([A-Za-zñáéíóú]+[\s]*){3,15}$/.test(name.value))){
        name.classList.add('red');
    }
    else{
        name.classList.remove('red');
    }
}
__('name').addEventListener("keyup", checkName);

function checkLastName() {
    var lname = __('lastname');
    if(!(/^([A-Za-zñáéíóú]+[\s]*){3,15}$/.test(lname.value))){
        lname.classList.add('red');
    }
    else{
        lname.classList.remove('red');
    }
}
__('lastname').addEventListener("keyup", checkLastName);

function checkEmail() {
    var mail = __('email');
    if(!(/^[^@]+@[^@]+\.[a-zA-Z]{2,5}$/.test(mail.value))){
        mail.classList.add('red');
    }
    else{
        mail.classList.remove('red');
    }
}
__('email').addEventListener("keyup", checkEmail);

function checkPassword() {
    var password = __('password');
    if(!(/^([A-Za-zñáéíóú0-9]+[\s]*){8,15}$/.test(password.value))){
        password.classList.add('red');
    }
    else{
        password.classList.remove('red');
    }
}
__('password').addEventListener("keyup", checkPassword);

function checkConfirm() {
    var confirm = __('confirm');
    var password = __('password');
    if(confirm.value != password.value){
        confirm.classList.add('red');
    }
    else{
        confirm.classList.remove('red');
    }
}
__('confirm').addEventListener("keyup", checkConfirm);

function validation(){
    var name = __('name'),
        lname = __('lastname'),
        mail = __('email'),
        password = __('password'),
        confirm = __('confirm');

    if(!(/^([A-Za-zñáéíóú]+[\s]*){3,15}$/.test(name.value))){
        alert('Debes escribir un nombre válido.');
        return false;
    }
    else if(!(/^([A-Za-zñáéíóú]+[\s]*){3,15}$/.test(lname.value))){
        alert('Debes escribir un apellido válido.');
        return false;
    }
    else if(!(/^[^@]+@[^@]+\.[a-zA-Z]{2,5}$/.test(mail.value))){
        alert('Debes escribir un correo electrónico válido.');
        return false;
    }
    else if(!(/^([A-Za-zñáéíóú0-9]+[\s]*){8,15}$/.test(password.value))){
        alert('Debes ingresar una contraseña válida.');
        return false;
    }
    else if(confirm.value != password.value){
        alert('Las contraseñas no coinciden.');
        return false;
    }
    let nombre = name.value
    let apellido = lname.value
    let correo = mail.value
    setCookie('Name', nombre, 30);
    setCookie('LastName', apellido, 30);
    setCookie('Mail', correo, 30);
    alert('Te has registrado correctamente.');
    return true;
}

function setCookie(setName, setValue, expiration){
    var date = new Date();
    date.setTime(date.getTime()+expiration*24*60*60*1000);
    var expiration = 'expires='+date.toUTCString();
    document.cookie = setName+'='+setValue+';'+expiration+';path=/';
}

function getCookie(getName){
    var cookieName = getName+'=';
    var array = document.cookie.split(';');
    for(let i=0; i<array.length; i++){
        var cookie = array[i];
        while (cookie.charAt(0)==' '){
            cookie = cookie.substring(1);
        }
        if(cookie.indexOf(getName)==0){
            return cookie.substring(cookieName.length, cookie.length);
        }
    }
    return '';
}

let names = getCookie('Name')
let lnames = getCookie('LastName')
let mails = getCookie('Mail')

let userName = document.getElementById('namepp')
let userLname = document.getElementById('lnamepp')
let userEmail = document.getElementById('emailpp')

userName.innerHTML = `<span> ${names} </span>`
userLname.innerHTML = `<span> ${lnames} </span>`
userEmail.innerHTML = `<span> ${mails} </span>`