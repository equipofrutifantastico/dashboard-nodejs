const app = require('./config/config')
const path = require('path')

require('./database/register')(app)
require('./database/login')(app)


// routes
const routes = require('./routes/index.routes')
app.use(routes)

const routesRegister = require('./routes/register.routes')
app.use(routesRegister)

const routesLogin = require('./routes/login.routes')
app.use(routesLogin)

const routesMain = require('./routes/main.routes')
app.use(routesMain)

const routesStock = require('./routes/stock.routes')
app.use(routesStock)

const routesSales = require('./routes/sales.routes')
app.use(routesSales)

const routesTransaction = require('./routes/transaction.routes')
app.use(routesTransaction)

const routesVisit = require('./routes/visit.routes')
app.use(routesVisit)

const routesTopSales = require('./routes/topsales.routes')
app.use(routesTopSales)

const routesProfile = require('./routes/profile.routes')
app.use(routesProfile)



// 404 error
app.use((req, res) => {
    res.sendFile(path.join(__dirname,'../public/404.html'))
})

//settings
app.set('port', process.env.PORT || 3000)

app.listen(app.get('port'), ()=> {
    console.log(`Servidor corriendo en el puerto ${app.get('port')}`)
})


