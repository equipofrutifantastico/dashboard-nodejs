const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')

const app = express()

// asignar motor de plantilla
app.set('view engine', 'pug')
app.set('views', path.join(__dirname,'../views'))

// middleware
app.use(bodyParser.urlencoded({extended: false}))

// static files
app.use(express.static(path.join(__dirname,'../../public')))

module.exports = app