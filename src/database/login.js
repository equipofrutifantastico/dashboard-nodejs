const dbConnection = require('./connection')
const bcrypt = require('bcrypt')

module.exports = app => {
    const connection = dbConnection

    app.post('/login', (req, res) => {
        const email = req.body.email
        const password = req.body.password

        if (email && password) {
            connection.query('SELECT password FROM usuario WHERE email = ?', [email],
                (err, result, fields) => {
                    if (bcrypt.compareSync(password, result[0].password)) {
                        res.redirect('/main')
                    } else {
                        res.send('Email o Password incorrecta')
                    }
                    res.end()
                })
        } else {
            res.send('Ingresa tu correo y contraseña')
            res.end()
        }
    })
}