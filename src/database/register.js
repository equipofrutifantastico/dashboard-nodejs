const dbConnection = require('./connection')
const bcrypt = require('bcrypt')

module.exports = app => {
    const connection = dbConnection

    app.post('/register', (req, res) => {
        //const password = req.body.password
        const {
            name,
            lastname,
            email,
        } = req.body
        const password = bcrypt.hashSync(req.body.password, 8)
        connection.query('INSERT INTO usuario SET ?', {
            name, lastname, email, password
        }, (err, result) => {
            res.redirect('/login')
        })
    })
}