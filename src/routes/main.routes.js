const express = require('express')
const router = express.Router()
const controller = require('../controllers/main.controller')

router.get('/main', controller.main)

module.exports = router 