const express = require('express')
const router = express.Router()
const controller = require('../controllers/sales.controller')

router.get('/sales', controller.sales)

module.exports = router 