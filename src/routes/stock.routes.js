const express = require('express')
const router = express.Router()
const controller = require('../controllers/stock.controller')

router.get('/stock', controller.stock)

module.exports = router