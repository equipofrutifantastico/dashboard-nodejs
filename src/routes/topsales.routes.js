const express = require('express')
const router = express.Router()
const controller = require('../controllers/topsales.controller')

router.get('/topsales', controller.topsales)

module.exports = router