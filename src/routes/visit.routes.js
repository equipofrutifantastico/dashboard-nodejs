const express = require('express')
const router = express.Router()
const controller = require('../controllers/visit.controller')

router.get('/visit', controller.visit)

module.exports = router